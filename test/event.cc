// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <terminnanny/event.h>

#include <string>
#include <vector>

#include <terminnanny/datetime.h>

using terminnanny::event;
using terminnanny::datetime;
using std::string;
using std::vector;

const string ex_sprechstunde = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//Nextcloud calendar v1.7.1\nCALSCALE:GREGORIAN\nBEGIN:VTIMEZONE\nTZID:Europe/Berlin\nBEGIN:DAYLIGHT\nTZOFFSETFROM:+0100\nTZOFFSETTO:+0200\nTZNAME:CEST\nDTSTART:19700329T020000\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\nEND:DAYLIGHT\nBEGIN:STANDARD\nTZOFFSETFROM:+0200\nTZOFFSETTO:+0100\nTZNAME:CET\nDTSTART:19701025T030000\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\nEND:STANDARD\nEND:VTIMEZONE\nBEGIN:VEVENT\nCREATED:20191008T195114\nDTSTAMP:20191008T195114\nLAST-MODIFIED:20191008T195114\nUID:R7TGMWB1WHN9XST1JGFT\nSUMMARY:Piratensprechstunde\nCLASS:PUBLIC\nSTATUS:CONFIRMED\nDTSTART;TZID=Europe/Berlin:20191011T160000\nDTEND;TZID=Europe/Berlin:20191011T170000\nRRULE:FREQ=WEEKLY;INTERVAL=2\nEND:VEVENT\nEND:VCALENDAR";

TEST_CASE("from ical") {
    // get repetitions, and duplicates
    datetime start(1610637916), end(1612051200);
    auto events_set = event::from_ical(ex_sprechstunde, start, end);
    REQUIRE(2 == events_set.size());

    for (const auto& e : events_set) {
        REQUIRE("Piratensprechstunde" == e.title);
        REQUIRE("" == e.location);
        REQUIRE("" == e.description);
    }

    // set is ordered -> make accessible via index
    vector<event> events(events_set.begin(), events_set.end());
    REQUIRE(1610722800 == events[0].start.get_time_t_utc());
    REQUIRE(1611932400 == events[1].start.get_time_t_utc());
}

TEST_CASE("from icals") {
    datetime start(1610637916), end(1611100800);

    // expect to remove duplicate ical
    auto events_set = event::from_icals({ex_sprechstunde, ex_sprechstunde}, start, end);
    REQUIRE(1 == events_set.size());

    for (const auto& e : events_set) {
        REQUIRE("Piratensprechstunde" == e.title);
        REQUIRE("" == e.location);
        REQUIRE("" == e.description);
    }

    // set is ordered -> make accessible via index
    vector<event> events(events_set.begin(), events_set.end());
    REQUIRE(1610722800 == events[0].start.get_time_t_utc());
}

TEST_CASE("operators") {
    event ev_1a, ev_1b, ev_2a, ev_3a;

    ev_1a.start = datetime(123);
    ev_1b.start = datetime(123);
    ev_2a.start = datetime(124);
    ev_3a.start = datetime(125);

    // op ==
    REQUIRE( (ev_1a == ev_1a));
    REQUIRE( (ev_1a == ev_1b));
    REQUIRE(!(ev_1a == ev_2a));
    REQUIRE(!(ev_1a == ev_3a));

    REQUIRE( (ev_1b == ev_1a));
    REQUIRE( (ev_1b == ev_1b));
    REQUIRE(!(ev_1b == ev_2a));
    REQUIRE(!(ev_1b == ev_3a));

    REQUIRE(!(ev_2a == ev_1a));
    REQUIRE(!(ev_2a == ev_1b));
    REQUIRE( (ev_2a == ev_2a));
    REQUIRE(!(ev_2a == ev_3a));

    REQUIRE(!(ev_3a == ev_1a));
    REQUIRE(!(ev_3a == ev_1b));
    REQUIRE(!(ev_3a == ev_2a));
    REQUIRE( (ev_3a == ev_3a));

    // op !=
    REQUIRE(!(ev_1a != ev_1a));
    REQUIRE(!(ev_1a != ev_1b));
    REQUIRE( (ev_1a != ev_2a));
    REQUIRE( (ev_1a != ev_3a));

    REQUIRE(!(ev_1b != ev_1a));
    REQUIRE(!(ev_1b != ev_1b));
    REQUIRE( (ev_1b != ev_2a));
    REQUIRE( (ev_1b != ev_3a));

    REQUIRE( (ev_2a != ev_1a));
    REQUIRE( (ev_2a != ev_1b));
    REQUIRE(!(ev_2a != ev_2a));
    REQUIRE( (ev_2a != ev_3a));

    REQUIRE( (ev_3a != ev_1a));
    REQUIRE( (ev_3a != ev_1b));
    REQUIRE( (ev_3a != ev_2a));
    REQUIRE(!(ev_3a != ev_3a));

    // op <
    REQUIRE(!(ev_1a < ev_1a));
    REQUIRE(!(ev_1a < ev_1b));
    REQUIRE( (ev_1a < ev_2a));
    REQUIRE( (ev_1a < ev_3a));

    REQUIRE(!(ev_1b < ev_1a));
    REQUIRE(!(ev_1b < ev_1b));
    REQUIRE( (ev_1b < ev_2a));
    REQUIRE( (ev_1b < ev_3a));

    REQUIRE(!(ev_2a < ev_1a));
    REQUIRE(!(ev_2a < ev_1b));
    REQUIRE(!(ev_2a < ev_2a));
    REQUIRE( (ev_2a < ev_3a));

    REQUIRE(!(ev_3a < ev_1a));
    REQUIRE(!(ev_3a < ev_1b));
    REQUIRE(!(ev_3a < ev_2a));
    REQUIRE(!(ev_3a < ev_3a));

    // op >
    REQUIRE(!(ev_1a > ev_1a));
    REQUIRE(!(ev_1a > ev_1b));
    REQUIRE(!(ev_1a > ev_2a));
    REQUIRE(!(ev_1a > ev_3a));

    REQUIRE(!(ev_1b > ev_1a));
    REQUIRE(!(ev_1b > ev_1b));
    REQUIRE(!(ev_1b > ev_2a));
    REQUIRE(!(ev_1b > ev_3a));

    REQUIRE( (ev_2a > ev_1a));
    REQUIRE( (ev_2a > ev_1b));
    REQUIRE(!(ev_2a > ev_2a));
    REQUIRE(!(ev_2a > ev_3a));

    REQUIRE( (ev_3a > ev_1a));
    REQUIRE( (ev_3a > ev_1b));
    REQUIRE( (ev_3a > ev_2a));
    REQUIRE(!(ev_3a > ev_3a));

    // op <=
    REQUIRE( (ev_1a <= ev_1a));
    REQUIRE( (ev_1a <= ev_1b));
    REQUIRE( (ev_1a <= ev_2a));
    REQUIRE( (ev_1a <= ev_3a));

    REQUIRE( (ev_1b <= ev_1a));
    REQUIRE( (ev_1b <= ev_1b));
    REQUIRE( (ev_1b <= ev_2a));
    REQUIRE( (ev_1b <= ev_3a));

    REQUIRE(!(ev_2a <= ev_1a));
    REQUIRE(!(ev_2a <= ev_1b));
    REQUIRE( (ev_2a <= ev_2a));
    REQUIRE( (ev_2a <= ev_3a));

    REQUIRE(!(ev_3a <= ev_1a));
    REQUIRE(!(ev_3a <= ev_1b));
    REQUIRE(!(ev_3a <= ev_2a));
    REQUIRE( (ev_3a <= ev_3a));

    // op >=
    REQUIRE( (ev_1a >= ev_1a));
    REQUIRE( (ev_1a >= ev_1b));
    REQUIRE(!(ev_1a >= ev_2a));
    REQUIRE(!(ev_1a >= ev_3a));

    REQUIRE( (ev_1b >= ev_1a));
    REQUIRE( (ev_1b >= ev_1b));
    REQUIRE(!(ev_1b >= ev_2a));
    REQUIRE(!(ev_1b >= ev_3a));

    REQUIRE( (ev_2a >= ev_1a));
    REQUIRE( (ev_2a >= ev_1b));
    REQUIRE( (ev_2a >= ev_2a));
    REQUIRE(!(ev_2a >= ev_3a));

    REQUIRE( (ev_3a >= ev_1a));
    REQUIRE( (ev_3a >= ev_1b));
    REQUIRE( (ev_3a >= ev_2a));
    REQUIRE( (ev_3a >= ev_3a));
}
