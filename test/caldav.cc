// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <string>

#include <terminnanny/caldav.h>
#include <terminnanny/datetime.h>

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;
using std::string;
using terminnanny::caldav;
using terminnanny::datetime;

TEST_CASE("constructor, get & set") {
    // just check that that compiles
    caldav w_empty;
    caldav w_url("https://example.com/only");
    REQUIRE(string(w_url.get_uri().host) == "example.com");
    REQUIRE(string(w_url.get_uri().path) == "/only");

    datetime dt1(4), dt2(1273), dt3(17);
    caldav w("http://host/path", dt1, dt2);

    REQUIRE(w.get_period_start() == dt1);
    REQUIRE(w.get_period_end() == dt2);

    w.set_period_end(dt3);
    REQUIRE(w.get_period_start() == dt1);
    REQUIRE(w.get_period_end() == dt3);

    w.set_period(dt3, dt2);
    REQUIRE(w.get_period_start() == dt3);
    REQUIRE(w.get_period_end() == dt2);
}

TEST_CASE("uri parsing") {
    caldav w("http://host/path");

    REQUIRE("host" == string(w.get_uri().host));
    REQUIRE("/path" == string(w.get_uri().path));
    REQUIRE("http" == string(w.get_uri().scheme));
    REQUIRE(80 == w.get_uri().port);

    REQUIRE(w.parse_uri("http://host:8080/path?query#fragment"));
    REQUIRE("host" == string(w.get_uri().host));
    REQUIRE("/path" == string(w.get_uri().path));
    REQUIRE("http" == string(w.get_uri().scheme));
    REQUIRE(8080 == w.get_uri().port);

    REQUIRE(w.parse_uri("http://host"));
    REQUIRE("host" == string(w.get_uri().host));
    REQUIRE("/" == string(w.get_uri().path));

    REQUIRE(!w.parse_uri(""));
    REQUIRE(!w.parse_uri("justastring"));
}

TEST_CASE("build request") {
    datetime from(1585699200), to(1610628069);
    caldav w("http://host", from, to);

    string req = w.build_events_request_body();

    REQUIRE_THAT(req,
                 Contains("time-range") &&
                 Contains("urn:ietf:params:xml:ns:caldav") &&
                 Contains("20200401T000000Z") &&
                 Contains("20210114T124109Z") &&
                 Contains("VEVENT"));
}
