option(TERMINNANNY_INSTALL_BIN "install terminnanny binary" ON)

add_executable(terminnanny "${CMAKE_CURRENT_LIST_DIR}/terminnanny.cc")
target_compile_features(terminnanny PUBLIC cxx_std_11)
target_compile_options(terminnanny PRIVATE -Wall -Wextra -Wpedantic -Werror)
target_link_libraries(terminnanny PRIVATE "${SPDLOG_LIBS}")
target_link_libraries(terminnanny PUBLIC terminnanny_core)

if (TERMINNANNY_INSTALL_BIN)
    install(
        TARGETS terminnanny
        DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}"
        )
endif()
