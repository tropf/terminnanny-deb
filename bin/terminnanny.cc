// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <string>
#include <vector>
#include <ctime>
#include <cctype>
#include <chrono>
#include <regex>
#include <locale>
#include <argp.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <libxml++/libxml++.h>
#pragma GCC diagnostic pop

#include <terminnanny/cfg.h>
#include <terminnanny/caldav.h>
#include <terminnanny/extractor.h>
#include <terminnanny/event.h>
#include <terminnanny/formatter.h>

using namespace std;

using terminnanny::cfg;

int main(int argc, char** argv) {
    // init spdlog to stderr
    spdlog::default_logger()->sinks().clear();
    auto stderr_sink = make_shared<spdlog::sinks::stderr_sink_mt>();
    spdlog::default_logger()->sinks().push_back(stderr_sink);
    spdlog::set_level(spdlog::level::warn);

    // load locale
    std::locale::global(std::locale(""));

    cfg.parse_args(argc, argv);

    if (!cfg.is_valid()) {
        spdlog::critical("configuration is not valid: {}", cfg.get_validation_error());
        return 1;
    }

    terminnanny::caldav remote;

    if (!remote.parse_uri(cfg.url)) {
        spdlog::critical("could not parse uri {}, aborting");
        return 1;
    }

    remote.set_auth_user(cfg.auth_user);
    remote.set_auth_pass(cfg.auth_pass);
    remote.set_period(cfg.period_start(), cfg.period_end());

    if (!remote.request_events()) {
        spdlog::critical("requesting caldav interface failed, aborting");
        return 1;
    }

    terminnanny::extractor ext;
    try {
        ext.parse_string(remote.get_request_result());
    } catch (xmlpp::exception& e) {
        spdlog::critical("error during parsing of server response: {}", e.what());
        return 1;
    }

    auto events = terminnanny::event::from_icals(ext.get_icals(), cfg.period_start(), cfg.period_end());
    spdlog::info("extracted {} events total", events.size());

    terminnanny::formatter f;
    f.fill_from_cfg(cfg);

    spdlog::debug("preparations done, generating output");

    if (events.empty()) {
        if (!cfg.format_empty.empty()) {
            cout << f.get_replaced(cfg.format_empty) << endl;
        } else {
            spdlog::info("no events in specified period, but no empty format specified");
        }

        return 0;
    }

    if (!cfg.format_header.empty()) {
        cout << f.get_replaced(cfg.format_header) << endl;
    }

    if (!cfg.format_event.empty()) {
        for (const auto& e : events) {
            f.fill_from_event(e);
            cout << f.get_replaced(cfg.format_event) << endl;
        }
    }

    if (!cfg.format_footer.empty()) {
        cout << f.get_replaced(cfg.format_footer) << endl;
    }

    return 0;
}
