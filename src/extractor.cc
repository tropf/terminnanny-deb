// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/extractor.h>

#include <string>
#include <vector>
#include <regex>

#include <spdlog/spdlog.h>

using std::vector;
using std::string;

terminnanny::extractor::extractor() {
    // this *does not* influence the replacement of predefined entities, e.g. &lt; and alike. (see tests)
    // this is to prevent attacks via XML external entities
    // (did not find clear documentation on this)
    set_substitute_entities(false);
}

terminnanny::extractor::~extractor() {
    // nop
}

vector<string> terminnanny::extractor::get_icals() const {
    return icals_str;
}

vector<string> terminnanny::extractor::get_icals(const string& s) {
    if (!s.empty()) {
        parse_string(s);
    }

    return get_icals();
}

void terminnanny::extractor::parse_string(const string& s) {
    parse_chunk(s);
    finish_chunk_parsing();
}

string terminnanny::extractor::get_caldata_elem_name() const {
    return caldav_ns_prefix + ":calendar-data";
}

void terminnanny::extractor::on_start_document() {}

void terminnanny::extractor::on_end_document() {
    spdlog::info("extracted {} icals", icals_str.size());
}

void terminnanny::extractor::on_start_element(const ustring& name, const AttributeList& properties) {
    if ("" == caldav_ns_prefix) {
        for (const auto& property : properties) {
            std::regex xmlns_regex("xmlns:(.+)");
            std::smatch m;
            if (std::regex_match(property.name.raw(), m, xmlns_regex)) {
                if ("urn:ietf:params:xml:ns:caldav" == property.value) {
                    caldav_ns_prefix = m[1];
                    spdlog::debug("set caldav prefix to '{}'", caldav_ns_prefix);
                }
            }
        }
    }
    if (get_caldata_elem_name() == name) {
        collect_ical = true;
        current_ical = "";
    }
}

void terminnanny::extractor::on_end_element(const ustring& name) {
    if (collect_ical && get_caldata_elem_name() == name) {
        collect_ical = false;
        icals_str.push_back(current_ical);
        spdlog::debug("collected ical");
    }
}

void terminnanny::extractor::on_characters(const ustring& characters) {
    if (collect_ical) {
        current_ical += characters;
    }
}

void terminnanny::extractor::on_comment(const ustring&) {
    // nop
}

void terminnanny::extractor::on_warning(const ustring& text) {
    spdlog::warn("XML parser: {}", text.c_str());
}

void terminnanny::extractor::on_error(const ustring& text) {
    spdlog::error("XML parser: {}", text.c_str());
}

void terminnanny::extractor::on_fatal_error(const ustring& text) {
    spdlog::critical("XML parser: {}", text.c_str());
}
