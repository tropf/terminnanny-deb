// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/formatter.h>

#include <string>
#include <map>
#include <regex>

#include <spdlog/spdlog.h>

#include <terminnanny/event.h>
#include <terminnanny/cfg.h>

using std::string;
using std::map;
using std::regex;
using terminnanny::datetime;
using terminnanny::event;


string terminnanny::formatter::get_replaced(const string& s) const {
    string copy = s;
    replace(copy);
    return copy;
}

static bool next_matching_key(const string& haystack, map<string, string> needles, size_t searchstart, string& result) {
    string::size_type next_pos = string::npos;
    string next_match;

    for (const auto& it : needles) {
        // does not make sense to match empty string
        if ("" == it.first) {
            continue;
        }

        auto find_result = haystack.find(it.first, searchstart);
        if (string::npos != find_result && find_result < next_pos) {
            next_match = it.first;
            next_pos = find_result;
        }
    }

    if (string::npos != next_pos) {
        result = next_match;
        return true;
    }

    return false;
}

void terminnanny::formatter::replace(string& s) const {
    string::size_type pos = 0;
    string match;
    spdlog::trace("examining: {}", s);

    while (next_matching_key(s, replacements, pos, match)) {
        string::size_type match_pos = s.find(match, pos);
        spdlog::trace("matched at pos {}: {}", match_pos, match);
        const string replacement = replacements.at(match);

        s.replace(match_pos, match.size(), replacement);
        pos = match_pos + replacement.size();
    }

    spdlog::trace("replacing done, result: {}", s);
}

void terminnanny::formatter::fill_from_event(const event& e) {
    replacements["%TITLE%"] = get_stripped(e.title);
    replacements["%DESCRIPTION%"] = get_stripped(e.description);
    replacements["%LOCATION%"] = get_stripped(e.location);

    replacements["%DATE%"] = e.start.get_string_localtime(format_date);
    replacements["%TIME%"] = e.start.get_string_localtime(format_time);
    replacements["%DATETIME%"] = e.start.get_string_localtime(format_datetime);
}

void terminnanny::formatter::fill_from_cfg(const config& cfg) {
    format_date = cfg.format_date;
    format_time = cfg.format_time;
    format_datetime = cfg.format_datetime;

    replacements["%PERIOD_START_DATE%"] = cfg.period_start().get_string_localtime(format_date);
    replacements["%PERIOD_START_TIME%"] = cfg.period_start().get_string_localtime(format_time);
    replacements["%PERIOD_START_DATETIME%"] = cfg.period_start().get_string_localtime(format_datetime);

    replacements["%PERIOD_END_DATE%"] = cfg.period_end().get_string_localtime(format_date);
    replacements["%PERIOD_END_TIME%"] = cfg.period_end().get_string_localtime(format_time);
    replacements["%PERIOD_END_DATETIME%"] = cfg.period_end().get_string_localtime(format_datetime);
}

string terminnanny::formatter::get_stripped(const string& s) {
    return std::regex_replace(s, regex("^(?:\\s|\\n)*((?:\\S(?:.|\\n)*)?\\S)?(?:\\s|\\n)*$", std::regex_constants::ECMAScript), "$1");
}
