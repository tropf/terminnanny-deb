// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/relative_datetime.h>

#include <ctime>
#include <string>
#include <map>
#include <regex>

#include <spdlog/spdlog.h>

using std::string;
using std::stoi;
using std::regex_match;
using terminnanny::relative_datetime;

string terminnanny::relative_datetime::error::to_string() const {
    if (error_field::field_none == field && error_type::err_ok == type) {
        return "";
    }

    const std::map<relative_datetime::error_field, string> field_names = {
        {error_field::field_none, "unkown field"},
        {error_field::field_year, "year"},
        {error_field::field_month, "month"},
        {error_field::field_week, "week"},
        {error_field::field_day, "day"},
        {error_field::field_hour, "hour"},
        {error_field::field_minute, "minute"},
        {error_field::field_second, "second"},
    };
    const std::map<relative_datetime::error_type, string> err_type_names = {
        {error_type::err_ok, "no error"},
        {error_type::err_syntax, "incorrect syntax"},
        {error_type::err_range, "incorrect range"},
        {error_type::err_only_relative, "this field is only relative"},
    };

    return field_names.at(field) + ": " + err_type_names.at(type);
}

terminnanny::relative_datetime::error terminnanny::relative_datetime::get_validation_error() const {
    const std::regex format("([+-]?)(\\d+)");
    std::smatch m;

    if (!year.empty()) {
        if (!regex_match(year, m, format)) {
            return {error_field::field_year, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int year_i = stoi(m[2]);
            if (year_i < 1900) {
                return {error_field::field_year, error_type::err_range};
            }
        }
    }

    if (!month.empty()) {
        if (!regex_match(month, m, format)) {
            return {error_field::field_month, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int month_i = stoi(m[2]);
            if (month_i < 1 || month_i > 12) {
                return {error_field::field_month, error_type::err_range};
            }
        }
    }

    if (!day.empty()) {
        if (!regex_match(day, m, format)) {
            return {error_field::field_day, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int day_i = stoi(m[2]);
            if (day_i < 1 || day_i > 31) {
                return {error_field::field_day, error_type::err_range};
            }
        }
    }

    if (!week.empty()) {
        if (!regex_match(week, m, format)) {
            return {error_field::field_week, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            return {error_field::field_week, error_type::err_only_relative};
        }

        // if week is empty -> day may only be relative
        if (regex_match(day, m, format) && m[1].str().empty()) {
            // absolute value -> check range
            return {error_field::field_day, error_type::err_only_relative};
        }
    }

    if (!hour.empty()) {
        if (!regex_match(hour, m, format)) {
            return {error_field::field_hour, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int hour_i = stoi(m[2]);
            if (hour_i < 0 || hour_i > 23) {
                return {error_field::field_hour, error_type::err_range};
            }
        }
    }

    if (!minute.empty()) {
        if (!regex_match(minute, m, format)) {
            return {error_field::field_minute, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int minute_i = stoi(m[2]);
            if (minute_i < 0 || minute_i > 59) {
                return {error_field::field_minute, error_type::err_range};
            }
        }
    }

    if (!second.empty()) {
        if (!regex_match(second, m, format)) {
            return {error_field::field_second, error_type::err_syntax};
        }
        if (m[1].str().empty()) {
            // absolute value -> check range
            int second_i = stoi(m[2]);
            if (second_i < 0 || second_i > 59) {
                return {error_field::field_second, error_type::err_range};
            }
        }
    }

    
    return {error_field::field_none, error_type::err_ok};
}

bool terminnanny::relative_datetime::is_valid() const {
    return get_validation_error().to_string().empty();
}

void terminnanny::relative_datetime::tidy() {
    while(!is_valid()) {
        auto error_in_field = get_validation_error().field;
        switch (error_in_field) {
        case error_field::field_year:
            year = "";
            break;
        case error_field::field_month:
            month = "";
            break;
        case error_field::field_week:
            week = "";
            break;
        case error_field::field_day:
            day = "";
            break;
        case error_field::field_hour:
            hour = "";
            break;
        case error_field::field_minute:
            minute = "";
            break;
        case error_field::field_second:
            second = "";
            break;
        default:
            spdlog::error("encountered error, but no field could be changed");
            return;
        }
    }
}

bool terminnanny::relative_datetime::is_empty() const {
    return year.empty() && month.empty() && day.empty() && week.empty() && hour.empty() && minute.empty() && second.empty();
}

bool terminnanny::relative_datetime::is_only_relative() const {
    const std::regex relative_regex("[+-]\\d+");
    return (year.empty() || regex_match(year, relative_regex)) &&
        (month.empty() || regex_match(month, relative_regex)) &&
        (day.empty() || regex_match(day, relative_regex)) &&
        (week.empty() || regex_match(week, relative_regex)) &&
        (hour.empty() || regex_match(hour, relative_regex)) &&
        (minute.empty() || regex_match(minute, relative_regex)) &&
        (second.empty() || regex_match(second, relative_regex));
}

bool terminnanny::operator==(const relative_datetime& lhs, const relative_datetime& rhs) {
    return lhs.year == rhs.year &&
        lhs.month == rhs.month &&
        lhs.day == rhs.day &&
        lhs.week == rhs.week &&
        lhs.hour == rhs.hour &&
        lhs.minute == rhs.minute &&
        lhs.second == rhs.second &&
        lhs.week_starts_monday == rhs.week_starts_monday;
}

bool terminnanny::operator!=(const relative_datetime& lhs, const relative_datetime& rhs) {
    return !(lhs == rhs);
}
