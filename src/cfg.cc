// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/cfg.h>

#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <regex>
#include <map>
#include <cerrno>
#include <argp.h>

#include <spdlog/spdlog.h>

#include <terminnanny/config.h>

using std::string;

// init global config empty
terminnanny::config terminnanny::cfg;

const char* argp_program_version = TERMINNANNY_VERSION;
const char* argp_program_bug_address = TERMINNANNY_BUG_ADDRESS;

// the compiler will complain that some fields are implicitly set to 0
// usually i would comply, but argp is designed around this.
// so for this section *only*, the warnings will be disabled

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const struct argp_option terminnanny::config::options[]{
    {0, 0, 0, 0, "Connection Options"},
    {"url", argp_options::url_option, "URL", 0, "URL to connect to"},
    {"pass", argp_options::pass, "PASS", 0, "password for authentication"},
    {"user", argp_options::user, "USER", 0, "username for authentication"},

    {0, 0, 0, 0, "Date Range Options\nEach option can be relative or absolute. For details and exceptions please refer to the man page."},
    {"from-year", argp_options::from_year, "YEAR", 0, "period start year (>=1900)"},
    {"from-month", argp_options::from_month, "MONTH", 0, "period start month (1-12)"},
    {"from-week", argp_options::from_week, "WEEK", 0, "period start week (only relative)"},
    {"from-day", argp_options::from_day, "DAY", 0, "period start day (1-31)"},
    {"from-hour", argp_options::from_hour, "HOUR", 0, "period start hour (0-23)"},
    {"from-minute", argp_options::from_minute, "MINUTE", 0, "period start minute (0-59)"},
    {"from-second", argp_options::from_second, "SECOND", 0, "period start second (0-59)"},
    {"to-year", argp_options::to_year, "YEAR", 0, "period end year (>=1900)"},
    {"to-month", argp_options::to_month, "MONTH", 0, "period end month (1-12)"},
    {"to-week", argp_options::to_week, "WEEK", 0, "period end week (only relative)"},
    {"to-day", argp_options::to_day, "DAY", 0, "period end day (1-31)"},
    {"to-hour", argp_options::to_hour, "HOUR", 0, "period end hour (0-23)"},
    {"to-minute", argp_options::to_minute, "MINUTE", 0, "period end minute (0-59)"},
    {"to-second", argp_options::to_second, "SECOND", 0, "period end second (0-59)"},
    {"delta-year", argp_options::delta_year, "YEAR", 0, "period duration year (only relative)"},
    {"delta-month", argp_options::delta_month, "MONTH", 0, "period duration month (only relative)"},
    {"delta-week", argp_options::delta_week, "WEEK", 0, "period duration week (only relative)"},
    {"delta-day", argp_options::delta_day, "DAY", 0, "period duration day (only relative)"},
    {"delta-hour", argp_options::delta_hour, "HOUR", 0, "period duration hour (only relative)"},
    {"delta-minute", argp_options::delta_minute, "MINUTE", 0, "period duration minute (only relative)"},
    {"delta-second", argp_options::delta_second, "SECOND", 0, "period duration second (only relative)"},

    {0, 0, 0, 0, ""},
    {"from-epoch", argp_options::from_epoch, "EPOCH", 0, "specify period start as unix epoch (timestamp), may not be combined with any other --from-[...] options"},
    {"to-epoch", argp_options::to_epoch, "EPOCH", 0, "specify period end as unix epoch (timestamp), may not be combined with any other --to-[...] or --delta-[...] options"},
    {"week-start-monday", argp_options::week_start_monday, 0, 0, "weeks start on monday (default)"},
    {"week-start-sunday", argp_options::week_start_sunday, 0, 0, "weeks start on sunday"},


    {0, 0, 0, 0, "Formatting options\nFor available replacements please refer to the man page."},
    {"format-header", argp_options::format_header_opt, "CONTEXTFORMAT", 0, "initial line printed before event list"},
    {"format-event", argp_options::format_event_opt, "EVENTFORMAT", 0, "printed for each event, followed by a newline"},
    {"format-footer", argp_options::format_footer_opt, "CONTEXTFORMAT", 0, "line printed after all events"},
    {"format-empty", argp_options::format_empty_opt, "CONTEXTFORMAT", 0, "only line printed if no events are in the calendar"},
    {"format-datetime", argp_options::format_datetime_opt, "DTFORMAT", 0, "format used for printing date & time"},
    {"format-date", argp_options::format_date_opt, "DTFORMAT", 0, "format used for printing date"},
    {"format-time", argp_options::format_time_opt, "DTFORMAT", 0, "format used for printing time"},

    {0, 0, 0, 0, "Other Options", -1},
    {"verbosity", argp_options::verbosity, "VERBOSITY", 0, "set verbosity, can be one of TRACE, DEBUG, INFO, WARN, ERR, CRITICAL"},
    {"cfg-file", argp_options::cfg_file, "FILE", 0, "try to load config from given file"},
    {0}
};
#pragma GCC diagnostic pop

static int parse_opt_wrapper(int key, char* arg, struct argp_state* state) {
    // argp requires a c-style function to be called.
    // this is a wrapper to call the appropriate member function of terminnanny::config

    return ((terminnanny::config*) state->input)->parse_single_opt(key, arg, state);
}

int terminnanny::config::parse_single_opt(int key, const char* arg, struct argp_state*) {
    string arg_s = "";
    if (nullptr != arg) {
        arg_s = arg;
    }
    spdlog::trace("loading key {} with content: {}", key, arg_s);
    string arg_s_upper = arg_s;
    transform(arg_s_upper.begin(), arg_s_upper.end(), arg_s_upper.begin(), ::toupper);

    const std::map<string, spdlog::level::level_enum> lvl_by_string = {
        {"TRACE", spdlog::level::trace},
        {"DEBUG", spdlog::level::debug},
        {"INFO", spdlog::level::info},
        {"WARN", spdlog::level::warn},
        {"ERR", spdlog::level::err},
        {"CRITICAL", spdlog::level::critical},
    };

    switch(key) {
    case argp_options::url_option:
        url = arg_s;
        break;
    case argp_options::pass:
        auth_pass = arg_s;
        break;
    case argp_options::user:
        auth_user = arg_s;
        break;
    case argp_options::verbosity:
        if (lvl_by_string.find(arg_s_upper) == lvl_by_string.end()) {
            spdlog::error("unknown log level '{}'", arg_s);
            return EINVAL;
        }
        spdlog::set_level(lvl_by_string.at(arg_s_upper));
        break;

    case argp_options::from_year:
        rel_start.year = arg_s;
        break;
    case argp_options::from_month:
        rel_start.month = arg_s;
        break;
    case argp_options::from_week:
        rel_start.week = arg_s;
        break;
    case argp_options::from_day:
        rel_start.day = arg_s;
        break;
    case argp_options::from_hour:
        rel_start.hour = arg_s;
        break;
    case argp_options::from_minute:
        rel_start.minute = arg_s;
        break;
    case argp_options::from_second:
        rel_start.second = arg_s;
        break;

    case argp_options::to_year:
        rel_end.year = arg_s;
        break;
    case argp_options::to_month:
        rel_end.month = arg_s;
        break;
    case argp_options::to_week:
        rel_end.week = arg_s;
        break;
    case argp_options::to_day:
        rel_end.day = arg_s;
        break;
    case argp_options::to_hour:
        rel_end.hour = arg_s;
        break;
    case argp_options::to_minute:
        rel_end.minute = arg_s;
        break;
    case argp_options::to_second:
        rel_end.second = arg_s;
        break;

    case argp_options::delta_year:
        rel_delta.year = arg_s;
        break;
    case argp_options::delta_month:
        rel_delta.month = arg_s;
        break;
    case argp_options::delta_week:
        rel_delta.week = arg_s;
        break;
    case argp_options::delta_day:
        rel_delta.day = arg_s;
        break;
    case argp_options::delta_hour:
        rel_delta.hour = arg_s;
        break;
    case argp_options::delta_minute:
        rel_delta.minute = arg_s;
        break;
    case argp_options::delta_second:
        rel_delta.second = arg_s;
        break;

    case argp_options::from_epoch:
        start_timestamp = arg_s;
        break;
    case argp_options::to_epoch:
        end_timestamp = arg_s;
        break;

    case argp_options::week_start_monday:
        rel_start.week_starts_monday = true;
        rel_end.week_starts_monday = true;
        rel_delta.week_starts_monday = true;
        break;
    case argp_options::week_start_sunday:
        rel_start.week_starts_monday = false;
        rel_end.week_starts_monday = false;
        rel_delta.week_starts_monday = false;
        break;

    case argp_options::format_header_opt:
        format_header = arg_s;
        break;
    case argp_options::format_event_opt:
        format_event = arg_s;
        break;
    case argp_options::format_footer_opt:
        format_footer = arg_s;
        break;
    case argp_options::format_empty_opt:
        format_empty = arg_s;
        break;
    case argp_options::format_datetime_opt:
        format_datetime = arg_s;
        break;
    case argp_options::format_date_opt:
        format_date = arg_s;
        break;
    case argp_options::format_time_opt:
        format_time = arg_s;
        break;

    case argp_options::cfg_file:
        return !parse_file(arg_s);
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

string terminnanny::config::get_validation_error() const {
    if (!rel_start.is_valid()) {
        return "start value: " + rel_start.get_validation_error().to_string();
    }
    if (!rel_end.is_valid()) {
        return "end value: " + rel_end.get_validation_error().to_string();
    }
    if (!rel_delta.is_valid()) {
        return "delta: " + rel_delta.get_validation_error().to_string();
    }

    if (!rel_delta.is_only_relative()) {
        return "delta may only be relative";
    }

    if (url.empty()) {
        return "url must not be empty";
    }

    if (rel_delta.is_empty() && rel_end.is_empty() && end_timestamp.empty()) {
        return "both absolute end and delta to start missing";
    }

    if (!rel_delta.is_empty() && (!rel_end.is_empty() || !end_timestamp.empty())) {
        return "only period end *xor* delta may be specified";
    }

    if (!start_timestamp.empty() && !rel_start.is_empty()) {
        return "only timestamp *xor* components may be given for start";
    }

    if (!end_timestamp.empty() && !rel_end.is_empty()) {
        return "only timestamp *xor* components may be given for end";
    }

    const std::regex only_digits("\\d*");
    if (!std::regex_match(end_timestamp, only_digits)) {
        return "end timestamp may only contain digits";
    }
    if (!std::regex_match(start_timestamp, only_digits)) {
        return "start timestamp may only contain digits";
    }

    if (period_end() <= period_start()) {
        return "end must be *after* start";
    }

    return "";
}

bool terminnanny::config::is_valid() const {
    return get_validation_error().empty();
}

void terminnanny::config::try_parse_args(int argc, char** argv, unsigned argp_flags) {
    if (!parse_args(argc, argv, argp_flags)) {
        spdlog::critical("could not parse args to valid configuration");
        exit(1);
    }
}

bool terminnanny::config::parse_args(int argc, char** argv, unsigned argp_flags) {
    if (0 == argc) {
        spdlog::error("parse_args has been provided 0 arguments, aborting");
        return false;
    }
    spdlog::trace("trying to parse {} args", argc);
    
    struct argp argp = {
        options,
        //passed this object via input, then calls the parse_single_opt() member function
        parse_opt_wrapper,
        "",
        "Fetches and prints events from a given range from the given calendar in an arbitrary format\vFor more details and examples please refer to the man page.",
        nullptr, nullptr, nullptr // make gcc shut up about unitialized members
    };

    // returns int, cast to bool
    return !argp_parse(&argp, argc, argv, argp_flags, 0, this);
}

terminnanny::datetime terminnanny::config::period_start() const {
    if (!start_timestamp.empty()) {
        if (!rel_start.is_empty()) {
            spdlog::warn("both timestamp & component-wise spec for start given. trying timestamp {}", start_timestamp);
        }

        try {
            spdlog::debug("building start datetime from timestamp");
            terminnanny::datetime dt_from_timestamp(std::stoull(start_timestamp));
            return dt_from_timestamp;
        } catch (std::invalid_argument&) {
            spdlog::warn("could not parse start timestamp, carrying on");
        } catch (std::out_of_range&) {
            spdlog::warn("could not parse start timestamp, carrying on");
        }
    }

    spdlog::debug("building start datetime from components");
    datetime dt_start = dt_init;

    dt_start.apply_relative_datetime_localtime(rel_start);
    return dt_start;
}

terminnanny::datetime terminnanny::config::period_end() const {
    if (!end_timestamp.empty()) {
        if (!rel_end.is_empty() || !rel_delta.is_empty()) {
            spdlog::warn("both timestamp & component-wise spec for end given. trying timestamp {}", end_timestamp);
        }

        try {
            spdlog::debug("building end datetime from timestamp");
            terminnanny::datetime dt_from_timestamp(std::stoull(end_timestamp));
            return dt_from_timestamp;
        } catch (std::invalid_argument&) {
            spdlog::warn("could not parse end timestamp, carrying on");
        } catch (std::out_of_range&) {
            spdlog::warn("could not parse end timestamp, carrying on");
        }
    }

    spdlog::debug("building end datetime from components");

    relative_datetime rel_used;
    datetime dt_base;
    bool floor_mode;

    if (!rel_delta.is_empty()) {
        if (!rel_end.is_empty()) {
            spdlog::warn("both duration & absolute ending are specified, using duration");
        }

        rel_used = rel_delta;
        dt_base = period_start();
        floor_mode = false;
    } else {
        rel_used = rel_end;
        dt_base = dt_init;
        floor_mode = true;
    }

    if (!rel_used.is_valid()) {
        spdlog::error("specification for end({}), ignoring errornous fields", rel_used.get_validation_error().to_string());
        rel_used.tidy();
    }

    dt_base.apply_relative_datetime_localtime(rel_used, floor_mode);
    return dt_base;
}

bool terminnanny::config::parse_file(const string& path) {
    std::ifstream f(path);

    if (!f.is_open()) {
        spdlog::error("could not open file '{}' for reading", path);
        return false;
    }

    std::stringstream ss;
    ss << f.rdbuf();

    if (!parse_lines(ss.str())) {
        spdlog::error("error during parsing of {} (see messages above)", path);
        return false;
    }

    return true;
}

bool terminnanny::config::parse_lines(const string& lines) {
    bool success = true;
    std::stringstream linestream(lines);

    int linenum = 1;
    for (string line; std::getline(linestream, line); linenum++) {
        spdlog::trace("parsing line {}: {}", linenum, line);

        // is comment?
        const std::regex comment_regex("\\s*#.*");
        if (std::regex_match(line, comment_regex)) {
            spdlog::trace("ignoring line {}: comment", linenum);
            continue;
        }

        // is empty?
        const std::regex empty_regex("\\s*");
        if (std::regex_match(line, empty_regex)) {
            spdlog::trace("ignoring line {}: empty", linenum);
            continue;
        }

        // normal line -> try to parse
        if (!parse_line(line)) {
            spdlog::error("could not parse line {} of cfg, carrying on", linenum);
            success = false;
        }
    }

    return success;
}

bool terminnanny::config::parse_line(const string& line) {
    const std::regex line_regex("\\s*(\\S+)(\\s+(\\S.*)?)?");
    std::smatch m;
    if (!std::regex_match(line, m, line_regex)) {
        spdlog::error("could not parse given line into key(+value)");
        return false;
    }

    string key_string = m[1];
    string value = m[3];

    for (const struct argp_option opt_struct : options) {
        if (nullptr != opt_struct.name && 0 == strcmp(opt_struct.name, key_string.c_str())) {
            // key found -> apply option
            // only 0 indicates success
            return 0 == parse_single_opt(opt_struct.key, value.c_str(), {});
        }
    }

    spdlog::error("could not find key {}", key_string);
    return false;
}
