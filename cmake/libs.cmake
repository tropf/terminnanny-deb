find_package(spdlog REQUIRED)

option(SPDLOG_FORCE_HEADER_ONLY "iff ON, will link against special spdlog header-only-target (may be header-only by default, depends on spdlog version)" OFF)
if (SPDLOG_FORCE_HEADER_ONLY)
    set(SPDLOG_LIBS "spdlog::spdlog_header_only")
else()
    set(SPDLOG_LIBS "spdlog::spdlog")
endif()

find_package(PkgConfig REQUIRED)
pkg_check_modules(LIBXML++ REQUIRED libxml++-2.6)
pkg_check_modules(LIBICAL REQUIRED libical)
pkg_check_modules(NEON REQUIRED neon)
