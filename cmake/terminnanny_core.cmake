configure_file("${PROJECT_SOURCE_DIR}/include/terminnanny/config.h.in" "${PROJECT_BINARY_DIR}/include/terminnanny/config.h" ESCAPE_QUOTES)

add_library(terminnanny_core STATIC
    include/terminnanny/datetime.h
    src/datetime.cc
    include/terminnanny/relative_datetime.h
    src/relative_datetime.cc
    include/terminnanny/cfg.h
    src/cfg.cc
    include/terminnanny/caldav.h
    src/caldav.cc
    include/terminnanny/extractor.h
    src/extractor.cc
    include/terminnanny/event.h
    src/event.cc
    include/terminnanny/formatter.h
    src/formatter.cc
    "${PROJECT_BINARY_DIR}/include/terminnanny/config.h"
    )
target_link_libraries(terminnanny_core PRIVATE
    "${SPDLOG_LIBS}"
    "${LIBXML++_LIBRARIES}"
    "${LIBICAL_LIBRARIES}"
    "${NEON_LIBRARIES}"
    )
target_compile_features(terminnanny_core PUBLIC cxx_std_14)
target_compile_options(terminnanny_core PRIVATE -Wall -Wextra -Wpedantic -Werror)
target_include_directories(terminnanny_core PUBLIC
    include/
    "${PROJECT_BINARY_DIR}/include/"
    "${LIBXML++_INCLUDE_DIRS}"
    "${LIBICAL_INCLUDE_DIRS}"
    "${NEON_INCLUDE_DIRS}"
    )
