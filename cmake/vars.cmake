# set these explicitly to allow usage when configuring files
set(CMAKE_INSTALL_PREFIX "/usr" CACHE STRING "Directory prefix when using install.")

# script assumes install prefixes of cpack/cmake to be equal
set(CPACK_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

include(GNUInstallDirs)
# if GNUInstallDirs reset CMAKE INSTALL PREFIX: put it back
if(NOT "${CMAKE_INSTALL_PREFIX}" EQUAL "${CPACK_INSTALL_PREFIX}")
    set(CMAKE_INSTALL_PREFIX "${CPACK_INSTALL_PREFIX}")
endif()

string(REPLACE "magic" "@" TERMINNANNY_BUG_ADDRESS "terminnanny" "magic" "tropf" "." "io")
set(TERMINNANNY_BUGTRACKER "https://codeberg.org/tropf/terminnanny")
