// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_EVENT_H_INCLUDED__
#define __TERMINNANNY_EVENT_H_INCLUDED__

#include <string>
#include <vector>
#include <set>

#include <terminnanny/datetime.h>

using terminnanny::datetime;
using std::string;

namespace terminnanny {
    class event {
    private:
        static const datetime nulldatetime;

    public:
        datetime start;
        string title;
        string location;
        string description;

        static std::set<event> from_ical(const string& ical, const datetime& from = nulldatetime, const datetime& to = nulldatetime);
        static std::set<event> from_icals(const std::vector<string>& icals, const datetime& from = nulldatetime, const datetime& to = nulldatetime);
    };

    bool operator<(const event& lhs, const event& rhs);
    bool operator>(const event& lhs, const event& rhs);
    bool operator<=(const event& lhs, const event& rhs);
    bool operator>=(const event& lhs, const event& rhs);
    bool operator==(const event& lhs, const event& rhs);
    bool operator!=(const event& lhs, const event& rhs);
}

#endif // __TERMINNANNY_EVENT_H_INCLUDED__
