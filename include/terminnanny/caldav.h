// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_CALDAV_H_INCLUDED__
#define __TERMINNANNY_CALDAV_H_INCLUDED__

#include <string>

#include <neon/ne_uri.h>

#include <terminnanny/datetime.h>

using std::string;

namespace terminnanny {
    class caldav {
    private:
        datetime from, to;
        string response_body;
        string auth_user, auth_pass;
        ne_uri uri;

    public:
        /// c++ date formatstring for absulte icaltime
        static const string ical_date_format;

        /// quick setter for init
        caldav(const string& _uri_str = "",
               const datetime& _from = datetime(),
               const datetime& _to = datetime(),
               const string& _auth_user = "",
               const string& _auth_pass = "");
        // destructor, frees memory
        ~caldav();

        /// set user for auth
        void set_auth_user(const string&);
        /// set pass for auth
        void set_auth_pass(const string&);

        /// return parsed uri construct
        ne_uri get_uri() const;
        /// parse uri, true on success
        bool parse_uri(const string&);

        /// set examined period, no validity checking
        void set_period(const datetime& _from, const datetime& _to);
        /// set period start, no validity checking
        void set_period_start(const datetime&);
        /// set period end, no validity checking
        void set_period_end(const datetime&);

        /// get period start
        datetime get_period_start() const;
        /// get period end
        datetime get_period_end() const;

        /// build caldav REPORT request for all events between the set dates
        string build_events_request_body() const;
        /// send request for events, true on success; memory leaks on failure
        bool request_events();
        /// returns result of request from request_events()
        string get_request_result() const;
    };
}

#endif // __TERMINNANNY_CALDAV_H_INCLUDED__
