// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_DATETIME_H_INCLUDED__
#define __TERMINNANNY_DATETIME_H_INCLUDED__

#include <ctime>
#include <string>

#include <terminnanny/relative_datetime.h>

namespace terminnanny {
    /**
     * Describes a point in time.
     * Will use UTC for anything, unless specified otherwise.
     * Can be constructed from timestamp, c-style tm (time struct) or "now".
     * Can consume a relative_datetime to change itself.
     */
    class datetime {
    private:
        time_t timestamp_utc;
        
    public:
        /// construct w/o data
        datetime();
        /// set from given timestamp in utc
        datetime(time_t);
        /// set from tm struct (in utc)
        datetime(const tm&);

        /// set from timestamp in utc
        void set_utc(time_t);
        /// set from tm struct in utc
        void set_utc(const tm&);
        /// set from tm struct in localtime
        void set_localtime(const tm&);

        /// apply difference from relative_datetime, use utc
        void apply_relative_datetime_utc(const relative_datetime&, bool ceil_mode = false);
        /// apply difference from relative_datetime, use utc
        void apply_relative_datetime_localtime(const relative_datetime&, bool ceil_mode = false);

        /// get as time_t in utc
        time_t get_time_t_utc() const;
        /// get as tm in utc
        tm get_tm_utc() const;
        /// get as tm in localtime
        tm get_tm_localtime() const;
        /// get as string in localtime
        std::string get_string_localtime(const std::string& format = "%c %Z") const;
        /// get as string in UTC
        std::string get_string_utc(const std::string& format = "%c %Z") const;

        /// get local offset to utc (== local - utc)
        static time_t get_utc_offset_sec();
    };

    bool operator<(const datetime& lhs, const datetime& rhs);
    bool operator>(const datetime& lhs, const datetime& rhs);
    bool operator==(const datetime& lhs, const datetime& rhs);
    bool operator!=(const datetime& lhs, const datetime& rhs);
    bool operator<=(const datetime& lhs, const datetime& rhs);
    bool operator>=(const datetime& lhs, const datetime& rhs);
}

#endif // __TERMINNANNY_DATETIME_H_INCLUDED__
