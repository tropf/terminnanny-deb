// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_RELATIVE_DATETIME_H_INCLUDED__
#define __TERMINNANNY_RELATIVE_DATETIME_H_INCLUDED__

#include <string>

using std::string;

namespace terminnanny {
    /**
     * Describes a relative point in time.
     * Syntax meaning:
     * +5 -> 5 units in the future
     * -2 -> 2 units in the past
     * 12 -> the absolute unit 12 (hour, minute, whatever)
     *
     * All components are optional.
     * When setting a component, all units of a smaller granularity are reset back to zero
     * Example: When setting the hour to "15" minutes and seconds are implictly set to 0 (while keeping year, month and day),
     * when day is set to "-0" it specifies midnight today.
     *
     * This behaviour when applying to a datetime is called "floor mode".
     *
     * If any component is given, and the relative datetime is applyed in "ceil mode", then all *unspecified* components *after* the less granular components are set to their maximum.
     * Example: When using +1 day in ceil mode describe the end of that day (23:59:59), while in floor mode day=+1 means the start of the next day 00:00:00
     * This is useful for specifying the end of periods, as "display until +1" is usually understood as "display everything *including* tomorrow", but in floor mode it would mean "everything before tomorrow". Humans have a complicated understanding of ranges.
     */
    struct relative_datetime {
        /// represents the field of an error
        enum struct error_field {
            field_none, field_year, field_month, field_day, field_week, field_hour, field_minute, field_second,
        };
        /// represents the kind of error
        enum struct error_type {
            err_ok, err_syntax, err_range, err_only_relative,
        };
        /// represents an error
        struct error {
            error_field field = error_field::field_none;
            error_type type = error_type::err_ok;
            /// renders the error to string
            string to_string() const;
        };

        /// year (>=1900)
        string year = "";
        /// month of the year (1-12)
        string month = "";
        /// day of the month (1-31)
        string day = "";
        /// only supports setting relative (!!)
        string week = "";
        /// hour of the day (0-23)
        string hour = "";
        /// minute of hour (0-59)
        string minute = "";
        /// seconds, no leap seconds (0-59)
        string second = "";
        /// whether the week starts on monday
        bool week_starts_monday = true;

        /// get first error (in machine-readable form)
        error get_validation_error() const;
        /// true iff all elements are set to valid values
        bool is_valid() const;
        /// true iff all components are relative (or not specified)
        bool is_only_relative() const;
        /// delete all invalid elements
        void tidy();
        /// no data has been set
        bool is_empty() const;
    };
    typedef struct relative_datetime relative_datetime;

    bool operator==(const relative_datetime& lhs, const relative_datetime& rhs);
    bool operator!=(const relative_datetime& lhs, const relative_datetime& rhs);
}

#endif // __TERMINNANNY_RELATIVE_DATETIME_H_INCLUDED__
