// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_EXTRACTOR_H_INCLUDED__
#define __TERMINNANNY_EXTRACTOR_H_INCLUDED__

#include <string>
#include <vector>

#include <spdlog/spdlog.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <libxml++/libxml++.h>
#pragma GCC diagnostic pop

using std::vector;
using std::string;
using ustring = Glib::ustring;

namespace terminnanny {
    class extractor : public xmlpp::SaxParser {
    public:
        extractor();
        ~extractor() override;

        /// parse an entire document
        void parse_string(const string& s);
        vector<string> get_icals() const;
        /// parse passed document
        vector<string> get_icals(const string& s);

    protected:
        string current_ical;
        bool collect_ical = false;
        string caldav_ns_prefix = "cal";
        vector<string> icals_str;

        string get_caldata_elem_name() const;

        void on_start_document() override;
        void on_end_document() override;
        void on_start_element(const ustring& name, const AttributeList& properties) override;
        void on_end_element(const ustring& name) override;
        void on_characters(const ustring& characters) override;
        void on_comment(const ustring& text) override;
        void on_warning(const ustring& text) override;
        void on_error(const ustring& text) override;
        void on_fatal_error(const ustring& text) override;
    };
}

#endif // __TERMINNANNY_EXTRACTOR_H_INCLUDED__
