.TH "terminnanny" "1" "@TERMINNANNY_VERSION_MONTH@ @TERMINNANNY_VERSION_YEAR@" "terminnanny" "User Commands"
.SH NAME
@PROJECT_NAME@ \- @PROJECT_DESCRIPTION@
.
.SH SYNOPSIS
.BR "terminnanny" " [\fI\,OPTION\/\fR...]"
.
.SH DESCRIPTION
.PP
Fetches and prints events from a given range from the given calendar in an arbitrary format.
.
.SS "General Operation"
.PP
When launched,
.B "terminnanny"
performs the following actions:
.
.IP "1)" 3
Parse arguments from commandlines, maybe load configuration from files.
Calculate the period start and end dates, validate the configuration.
.IP "2)"
Contact the server at the specified URL, send a caldav request and parse the returned information.
.IP "3)"
Extract all individual events, sort them and print them (see
.BR "Formatting" ")."
.
.PP
During operation information on the current state are printed to standard error.
Unless critical, errors will be reported & recovered from,
but can still cause unwanted behaviour so check standard error even after successful runs.
.
.SS "Setting dates"
.PP
All date-related operations are performed in regard to your current timezone.
See 
.B "ENVIRONMENT"
for further information.
Note also that only the start of events is considered,
their duration can exceed the specified period.
.
.PP
To invoke
.B "terminnanny"
you have to provide can provide two dates:
The period start date, the earliest point in time where an event will be displayed (optional),
given by the
.B "\-\-from\-[...]"
options;
and the period end date, the last point in time where an event will still be displayed (required),
given by 
.RB "\fIeither\fR the " "\-\-to\-[...]" " options \fIor\fR the " "\-\-delta\-[...]" " options."
.
.PP
When the
.B "\-\-[...]\-epoch"
options are not used, the dates can specified component-wise:
Each component can be empty,
set to an explicit value (for ranges see below) or to a relative value.
.
When using relative values, they options
.BR "\-\-from\-[...]" " and " "\-\-to\-[...]"
are relative to current time, while
.BR "\-\-delta\-[...]"
are relative to the start time given by
.BR "\-\-from\-[...]" "."
.
.PP
The units refer to the components of the wall time,
and do
.I "not"
refer to intervals in terms of "exactly x seconds long".
If it is now 16:23:17, setting the hour to +2 refers to 18:00:00,
even though this is less than 2 hours in the future.
Similarly setting the hour to +0 or -0 refers to the start of the current hour,
in this case 16:00:00.
.
.PP
Additionally, if
.I "ceil mode"
is used, after all components have been applied
all not-specified components from the least significant (with the least granularity)
to (excluding) the first specified component are
.IR "set to their maximum" "."
.
For example, if it is now 16:23:17, the hour +2 with ceil mode enabled refers to 18:59:59,
or setting month to +0 refers to the last day of the current month at 23:59:59.
.
Ceil mode is
.I "only"
used for the
.B "\-\-to\-[...]"
options;
.BR "\-\-from\-[...]" " and " "\-\-delta\-[...]"
.IR "are not affected" "."
.
.PP
For examples please see below.
.
.SS "Formatting"
.PP
After all events have been fetched, the formats specified by
.BR "\-\-format\-[...]"
will be used to print the results.
.
.PP
If there are no events in the specified period,
empty format will be printed, followed by a newline and
.B "terminnanny"
exits.
Otherwise, if specified the header format followed by newline will be printed once.
For each individual event the event format (if not empty) followed by a newline will be printed.
The events are in the ordered by their start date.
After all events have been printed, if a footer format has been specified it will be printed followed by a newline.
.
.PP
The options
.BR "\-\-format\-[date|time|datetime]"
specify the format used in the other replacements listed below.
For date & time formatting the format
.RI "(" "DTFORMAT" ")"
is passed to 
.I "std::put_time"
of C++11, so you can use any compatible replacements.
You can look up a full list of available replacements at
.B "strftime" "(3)."
Note that the text options depend on your locale.
.
.TS
nospaces tab(|);
lB lB lB
lB l l.
string | replaced by | example
_
%Y | year, 4 digits | 2021
%m | month, 2 digits | 08
%B | month, full name | March
%d | day of month, 2 digits | 28
%a | day of week, abbreviated | Fri
%A | day of week, full | Mittwoch
%H | hour, 2 digits | 17
%M | minutes, 2 digits | 33
%S | seconds, 2 digits | 00
.TE
.
.PP
For
.IR "CONTEXTFORMAT" ","
used by the options
.BR "\-\-format\-[header|footer|empty]"
the following replacements are available:
.
.TP
.B \en
print a newline 
.TP
.B \e\e
print a single \e 
.TP
.B %PERIOD_START_DATE%
earliest possible start of any event, formatted as date
.TP
.B %PERIOD_START_TIME%
earliest possible start of any event, formatted as time
.TP
.B %PERIOD_START_DATETIME%
earliest possible start of any event, formatted as datetime
.TP
.B %PERIOD_END_DATE%
last possible start of any event, formatted as date
.TP
.B %PERIOD_END_TIME%
last possible start of any event, formatted as time
.TP
.B %PERIOD_END_DATETIME%
last possible start of any event, formatted as datetime
.
.PP
In addition to these, in
.I "EVENTFORMAT"
as given by
.B "\-\-format\-event"
the following replacements are available.
Note that
.BR "%TITLE%" ", " "%DESCRIPTION%" " and " "%LOCATION%"
have all leading and trailing whitespaces (space, tab, newline) removed.
.
.TP
.B %TITLE%
title of the event
.TP
.B %DESCRIPTION%
description of the event
.TP
.B %LOCATION%
location of the event
.TP
.B %DATE%
start of the event, formatted as date
.TP
.B %TIME%
start of the event, formatted as time
.TP
.B %DATETIME%
start of the event, formatted as datetime
.
.
.SH OPTIONS
.SS Connection Options
.TP
\fB\-\-pass\fR=\fI\,PASS\/\fR
password for authentication
.TP
\fB\-u\fR, \fB\-\-url\fR=\fI\,URL\/\fR
URL to connect to
.TP
\fB\-\-user\fR=\fI\,USER\/\fR
username for authentication
.SS Date Range Options
.TP
\fB\-\-delta\-day\fR=\fI\,DAY\/\fR
period duration day (only relative)
.TP
\fB\-\-delta\-hour\fR=\fI\,HOUR\/\fR
period duration hour (only relative)
.TP
\fB\-\-delta\-minute\fR=\fI\,MINUTE\/\fR
period duration minute (only relative)
.TP
\fB\-\-delta\-month\fR=\fI\,MONTH\/\fR
period duration month (only relative)
.TP
\fB\-\-delta\-second\fR=\fI\,SECOND\/\fR
period duration second (only relative)
.TP
\fB\-\-delta\-week\fR=\fI\,WEEK\/\fR
period duration week (only relative)
.TP
\fB\-\-delta\-year\fR=\fI\,YEAR\/\fR
period duration year (only relative)
.TP
\fB\-\-from\-day\fR=\fI\,DAY\/\fR
period start day (1\-31)
.TP
\fB\-\-from\-hour\fR=\fI\,HOUR\/\fR
period start hour (0\-23)
.TP
\fB\-\-from\-minute\fR=\fI\,MINUTE\/\fR
period start minute (0\-59)
.TP
\fB\-\-from\-month\fR=\fI\,MONTH\/\fR
period start month (1\-12)
.TP
\fB\-\-from\-second\fR=\fI\,SECOND\/\fR
period start second (0\-59)
.TP
\fB\-\-from\-week\fR=\fI\,WEEK\/\fR
period start week (only relative)
.TP
\fB\-\-from\-year\fR=\fI\,YEAR\/\fR
period start year (>=1900)
.TP
\fB\-\-to\-day\fR=\fI\,DAY\/\fR
period end day (1\-31)
.TP
\fB\-\-to\-hour\fR=\fI\,HOUR\/\fR
period end hour (0\-23)
.TP
\fB\-\-to\-minute\fR=\fI\,MINUTE\/\fR
period end minute (0\-59)
.TP
\fB\-\-to\-month\fR=\fI\,MONTH\/\fR
period end month (1\-12)
.TP
\fB\-\-to\-second\fR=\fI\,SECOND\/\fR
period end second (0\-59)
.TP
\fB\-\-to\-week\fR=\fI\,WEEK\/\fR
period end week (only relative)
.TP
\fB\-\-to\-year\fR=\fI\,YEAR\/\fR
period end year (>=1900)
.TP
\fB\-\-from\-epoch\fR=\fI\,EPOCH\/\fR
specify period start as unix epoch (timestamp),
may not be combined with any other \fB\-\-from\-[...]\fR
options
.TP
\fB\-\-to\-epoch\fR=\fI\,EPOCH\/\fR
specify period end as unix epoch (timestamp), may
not be combined with any other \fB\-\-to\-[...]\fR or
\fB\-\-delta\-[...]\fR options
.TP
\fB\-\-week\-start\-monday\fR
weeks start on monday (default)
.TP
\fB\-\-week\-start\-sunday\fR
weeks start on sunday
.SS
Formatting options
.TP
\fB\-\-format\-date\fR=\fI\,DTFORMAT\/\fR format used for printing date
.TP
\fB\-\-format\-datetime\fR=\fI\,DTFORMAT\/\fR
format used for printing date & time
.TP
\fB\-\-format\-empty\fR=\fI\,CONTEXTFORMAT\/\fR
only line printed if no events are in the
calendar
.TP
\fB\-\-format\-event\fR=\fI\,EVENTFORMAT\/\fR
printed for each event, followed by a
newline
.TP
\fB\-\-format\-footer\fR=\fI\,CONTEXTFORMAT\/\fR
line printed after all events
.TP
\fB\-\-format\-header\fR=\fI\,CONTEXTFORMAT\/\fR
initial line printed before event list
.TP
\fB\-\-format\-time\fR=\fI\,DTFORMAT\/\fR format used for printing time
.SS Other Options
.TP
\fB\-?\fR, \fB\-\-help\fR
Give this help list
.TP
\fB\-c\fR, \fB\-\-cfg\-file\fR=\fI\,FILE\/\fR
try to load config from given file
.TP
\fB\-\-usage\fR
Give a short usage message
.TP
\fB\-v\fR, \fB\-\-verbosity\fR=\fI\,VERBOSITY\/\fR
set verbosity, can be one of TRACE, DEBUG, INFO, WARN, ERR, CRITICAL
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version.
The version corresponding to this man page would be @TERMINNANNY_VERSION@.
.SH EXIT STATUS
The only exit status indicating success is 0.
Any other exit status signals a failure.
.
.SH ENVIRONMENT
.PP
.B "terminnanny"
does respect you locale settings for date formatting and timezone,
which can also be set via environment variables.
The exact details depend on your local system, 
please refer to
.BR "environ" "(7)"
for further information, for details see
.BR "locale" "(1),  " "locale" "(7) and " "tzset" "(3)."
.
.PP
The quick-and-dirty method for the language is to simply set
.B "LC_ALL"
to your desired locale.
The used timezone also depends on your local system, you can start at
.B "localtime" "(5)"
to find further information.
To just temporarily change the timezone set the environment variable
.B "TZ"
to your desired value, e.g.
.BR "Europe/Berlin" "."
If your setup was successful,
.B "date" "(1)"
should display the current time in your desired timezone.
.
.SH FILES
.PP
.B "terminnanny"
can read configuration files.
This is highly recommended for passing credentials,
because the options (including the credentials) are displayed in the process list.
.
The format is best understood by a short look at this example:
.PP
.EX
.in +4n
# i'm a comment
     # comments can have any indentation
format-time %H:%M # this is not a comment, but part of the format
        format-header         any indentation is ignored
   week-start-monday
.EE
.in
.
.PP
All long options are valid keys, values must start with a non-space character.
If no value is provided, "" (empty string) is assumed.
For the sake of completeness here is a full definition in ABNF:
.PP
.EX
.in +4n
NLF     = %x01-09 / %x0B - %xFF
            ; anything except newline
NWSP    = %x01-08 / %x0B - %x1F / %x21 - %xFF
            ; anything except newline, space and tab
cfgfile = [ *(line LF) ] [ line ]
line    = *WSP [ content / comment ]
comment = "#" *NLF
content = key WSP *WSP value
key     = NWSP *NWSP
value   = NWSP *NLF
.EE
.in
.
.SH NOTES
.PP
The following libraries are used:
.
.ad l
.IP * 3
.UR "https://\:www.gnu.org/\:software/\:libc/\:manual/\:html_node/\:Argp.html"
Argp from the GNU C Library
.UE
(LGPLv2.1) for option parsing,
.IP *
.UR "https://\:libxmlplusplus.github.io/\:libxmlplusplus/"
libxml++
.UE
(LGPLv2+) for xml parsing,
.IP *
.UR "https://\:libical.github.io/\:libical/"
libical
.UE
(MPLv2.0) for ical parsing & processing,
.IP *
.UR "https://\:github.com/\:gabime/\:spdlog/"
spdlog
.UE
(MIT) for all console output,
.IP *
.UR "https://\:notroj.github.io/\:neon/"
neon
.UE
(LGPLv2+) for interacting with caldav servers,
.IP *
.UR "https://\:github.com/\:catchorg/\:Catch2/"
catch
.UE
(BSLv1.0) to run the tests.
.ad b
.
.SH EXAMPLES
.PP
I hate man pages without examples.
.
.SS "Commands"
.PP
events next week (today is 2020-01-15)
.PP
.EX
.in +4n
$ \fBterminnanny --url 'https://example.com/caldav/mycal/' --user alice --pass hackme --from-week +1 --delta-week +1\fR
2021-01-19 18:30:00 @ : Nerdy Tuesday
2021-01-20 17:00:00 @ : Baking together
2021-01-22 11:11:00 @ Location: Testtitle
2021-01-24 11:11:00 @ Location: Testtitle
.EE
.in

.PP
events during february and march (today is 2020)
.PP
.EX
.in +4n
$ \fBterminnanny --url 'https://example.com/caldav/mycal/' --user alice --pass hackme --from-month 2 --to-month 3\fR
2021-02-19 18:30:00 @ Clubhouse: Cooking Session
2021-02-20 17:00:00 @ Clubhouse: Beer Tasting
2021-03-12 14:00:00 @ Garden: Workshop Gardening
2021-03-24 19:00:00 @ Garden: Workshop Gardening (rerun)
.EE
.in
.
.PP
Create the file
.IR "calendar_cfg" ":"
.PP
.EX
.in +4n
# location & credentials
url https://example.com/caldav/othercal
user alice
pass hackme

# format for dates (for a german audience)
format-date %d.%m
format-time %H:%M Uhr
format-datetime %A, %d. %B um %H:%M Uhr

# format actual output
format-header Termine von %PERIOD_START_DATE% bis %PERIOD_END_DATE%
format-event %DATETIME% @ %LOCATION%\en**%TITLE%**\en%DESCRIPTION%\en
.EE
.in
.
.PP
Then you can simply call the following for all dates of the current month:
.
.PP
.EX
.in +4
$ \fBterminnanny -c calendar_cfg --from-month=-0 --to-month=-0\fR
Termine von 01.01 bis 31.01
Freitag, 01. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.

Dienstag, 05. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Dienstag, 12. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Freitag, 15. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.

Dienstag, 19. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Dienstag, 26. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Freitag, 29. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.
.EE
.in
.
.SS "Date Calculation"
.PP
Assume today is 2021-01-20 16:23:17, week start is set to monday.
For each of the examples, use
.I "only one of"
.BR "\-\-to\-[...]" " or " "\-\-delta\-[...]" "."
.
.TS
nospaces tab(|);
lB  lB  lB  lB  lB  lB  lB  lB  lB  lB
l  lB  r  r  r  r  r  r  r  l.
description | prefix | \-year | \-month | \-week | \-day | \-hour | \-minute | \-second | example result
=
events tomorrow | \-\-from  | | | | +1 | | | | 2021-01-21 00:00:00
                | \-\-to    | | | | +1 | | | | 2021-01-21 23:59:59
                | \-\-delta | | | | +1 | | | | 2021-01-22 00:00:00
_
events until end of march   | \-\-from  ||  |||||| 2021-01-20 16:23:17
                            | \-\-to    || 3|||||| 2021-03-31 23:59:59
                            | \-\-delta ||+3|||||| 2021-04-01 00:00:00
_
events during the next 3 weeks | \-\-from  |||+1||||| 2021-01-25 00:00:00
                               | \-\-to    |||+3||||| 2021-02-07 23:59:59
                               | \-\-delta |||+3||||| 2021-02-08 00:00:00
_
events until tuesday next week, 12:00 | \-\-from  |||||||| 2021-01-20 16:23:17
\fB!! WRONG WAY !!\fR                    | \-\-to |||+1|+1|12|| | 2021-01-26 \fB12:59:59\fR
right way: set seconds to 0           | \-\-to    |||+1|+1|12||0| 2021-01-26 12:00:00
(not recommended to use)              | \-\-delta |||+1|+1|-4|| | 2021-01-26 12:00:00
.TE
.
.SH REPORTING BUGS
.PP
Report bugs to @TERMINNANNY_BUG_ADDRESS@ or 
.UR "@TERMINNANNY_BUGTRACKER@"
online
.UE "."
.
.SH COPYRIGHT
.PP
Built by Rolf Pfeffertal
.MT "@TERMINNANNY_BUG_ADDRESS@"
.ME .
.
.PP
Available under the GNU GPLv3 or later.
You have received a copy of the GNU GPLv3 at:
.I "@TERMINNANNY_LICENSE_INSTALL_LOCATION@"
.
.SH SEE ALSO

